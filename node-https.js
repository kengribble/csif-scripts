var https = require('https');
var fs = require('fs');
var port = 8888;
 
var options = {
  key: fs.readFileSync('CERTS/key.pem'),
  cert: fs.readFileSync('CERTS/cert.pem')
};
 
var a = https.createServer(options, function (reguest, response) {
  response.writeHead(200);
  response.end("hello encrypted world!\n");
}).listen(port);
console.log("Running node with HTTPS on port:" + port);
console.log("Make sure your URL starts https:// and ends with :" + port);
console.log("Full Instructions at: https://goo.gl/x1ceVt");   

