# README #

## What is this repository for? ##

* A set of scripts to assist beginning programmers in the Computer Science department's instructional facility at UC Davis.
## Scripts ##

* pleasekillit - kills all or a users processes, except the one's that keep their login alive -- Version: Beta 1.0
* node-https.js -- a script that desmonstrates running node.js as HTTPS ( see https://goo.gl/x1ceVt for needed documentation )


## How do I get set up? ##
* perl is required for some scripts (pleasekillit)
* node.js is required for some scripts (node-https.js)
* don't forget to make the perl scripts executable 

## Use ##
### pleasekillit ###

$ pleasekillit (-v) (-d)

-v shows verbose information as pleasekillit is normally quiet

-d show debug information (more than verbose)

### node-https.js ###
 
see https://goo.gl/x1ceVt for needed documentation